<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use App\Models\Actor;
use App\Models\User;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $search = $request->input('search');

        $movies = Movie::where('title', 'like', "%$search%")
            ->orWhere('description', 'like', "%$search%")
            ->get();

        $actors = Actor::where('name', 'like', "%$search%")->get();

        $users = User::where('name', 'like', "%$search%")
            ->orWhere('email', 'like', "%$search%")
            ->get();

        $results = compact('search', 'movies', 'actors', 'users');
        return $results;

        // return view('search', compact('search', 'movies', 'actors', 'users'));
    }
}












