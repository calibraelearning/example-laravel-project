<?php

namespace App\Http\Controllers;

use App\Models\Actor;
use App\Models\Movie;
use App\Models\Rating;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

// Responsible for handling requests
// associated with the Movie model
class MovieController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:view,movie')->only('show');

        $this->middleware('can:create,' . Movie::class)->only(['create', 'store']);

        $this->middleware('can:update,movie')->only('attachMovieToActor');
    }

    public function index(Request $request)
    {
    	// extract search param from request
    	$search = $request->input('search');

    	// configure & execute the DB query
    	$movies = Movie::where('title', 'like', "%$search%")
    		->orderBy('title')
    		->paginate();

		// pass to the view for rendering!
    	return view('movies/index', compact('movies'));
    }

    public function show(Movie $movie)
    {
    	return view('movies/show', compact('movie'));
    }

    // Show the form for creating a new Movie
    public function create()
    {
        return view('movies/create');
    }

    public function store(Request $request)
    {
        $input = $request->validate([
            'title' => 'required|unique:movies|min:4|max:100',
            'description' => 'required|min:10'
        ]);

        $movie = Auth::user()->movies()->create($input);

        return redirect($movie->url());
    }

    public function storeRating(Request $request, Movie $movie)
    {
        $input = $request->validate([
            'score' => 'required|integer|min:1|max:5'
        ]);

        $movie->ratings()->create([
            'user_id' => 1,
            'score' => $input['score']
        ]);

        return redirect($movie->url());
    }

    public function search(Request $request)
    {
        $query = $request->input('query');
        $num = $request->input('num', config('search.num'));
        if ($num > config('search.max'))
            $num = config('search.max');

        return Movie::where('title', 'like', "%$query%")
            ->orWhere('description', 'like', "%$query%")
            ->orderBy('title')
            ->limit($num)
            ->get();
    }

    public function attachMovieToActor(Movie $movie, Actor $actor)
    {
        // if the actor is already associated with the movie
        if ($movie->actors()->where('actors.id', $actor->id)->exists())
            $response = response()->json([
                'message' => 'Already associated'
            ], 400);
        else
        {
            $movie->actors()->attach($actor->id);

            $movie->load('actors');

            $response = $movie;
        }

        return $response;
    }
}







