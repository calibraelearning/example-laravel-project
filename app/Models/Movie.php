<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    use HasFactory;

    // what props can be set via mass assignment
    protected $fillable = [
    	'title',
    	'description',
        'poster'
    ];

    protected $appends = [
        'url'
    ];

    public function ratings()
    {
        return $this->hasMany(Rating::class);
    }

    public function averageScore()
    {
        $tally = 0;
        foreach($this->ratings as $rating)
            $tally += $rating->score;

        return $this->ratings->count()
            ? $tally / $this->ratings->count()
            : 0;
    }

    public function actors()
    {
        return $this->belongsToMany(Actor::class);
    }

    public function url()
    {
    	return route('movies.show', $this->id);
    }

    public function getUrlAttribute()
    {
        return $this->url();
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}











