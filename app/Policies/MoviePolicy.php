<?php

namespace App\Policies;

use App\Models\Movie;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class MoviePolicy
{
    use HandlesAuthorization;

    public function view(User $user, Movie $movie)
    {
        return true; // $movie->owner == $user;
    }

    public function create(User $user)
    {
        // using a specific user id like this should not be done. Instead, check a user
        // for the presence of a particular role
        return $user->id == 1;
    }

    public function update(User $user, Movie $movie)
    {
        return $movie->id < 50;
    }

}
