require('./bootstrap');

import Vue from 'vue';

import { InertiaApp } from '@inertiajs/inertia-vue';
import { InertiaForm } from 'laravel-jetstream';
import PortalVue from 'portal-vue';
import ActorDetails from "./Components/ActorDetails";
import GenericInfo from "./Components/GenericInfo";
import Thumbnail from "./Components/Thumbnail";
import MovieList from "./Components/MovieList";
import MovieSearch from "./Components/MovieSearch";

// Jetstream components
Vue.use(InertiaApp);
Vue.use(InertiaForm);
Vue.use(PortalVue);

// Declare some global methods / features that will be available in all components
Vue.mixin({

    data: function () {
        return {
            moviesApi: window.movieApi,
            test: 'abc'
        };
    },

    methods: {
        localDateTimeString: function (date) {
            return new Date(date).toLocaleTimeString();
        }
    }
});

// register custom components
Vue.component('actor-details', ActorDetails);
Vue.component('generic-info', GenericInfo);
Vue.component('pic-thumbnail', Thumbnail);
Vue.component('movie-list', MovieList);
Vue.component('movie-search', MovieSearch);

const moviesApp = document.getElementById('movies-app');

new Vue({
}).$mount(moviesApp);











const app = document.getElementById('app');

new Vue({
    render: (h) =>
        h(InertiaApp, {
            props: {
                initialPage: JSON.parse(app.dataset.page),
                resolveComponent: (name) => require(`./Pages/${name}`).default,
            },
        }),
}).$mount(app);
