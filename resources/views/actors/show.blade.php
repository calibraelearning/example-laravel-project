@extends('layouts.master')

@section('content')

    <actor-details
        :actor="{{ $actor }}"
        ></actor-details>

@endsection
