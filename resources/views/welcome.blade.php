<html>
    <body>
        <h1>
            {{ $name }}
        </h1>
        <p>
            {{ $name }} is {{ $age }} years old
        </p>
        <p>
            They are {{ $height }} tall
        </p>
    </body>
</html>