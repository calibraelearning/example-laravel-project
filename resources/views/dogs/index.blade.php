<html>
	<head>
		<style type="text/css">
			img {
				max-width: 30px;
				max-height: 30px;
			}
		</style>
	</head>
	<body>

		
		<h1>{{ $num }} Dogs!</h1>

		@if($num <= $max)
			@include('dogs/dog-pics', compact('num'))
		@else
			<h3>
				Too many dogs!
			</h3>
		@endif
	</body>
</html>


