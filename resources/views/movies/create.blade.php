@extends('layouts/master')

@section('content')

	<div class="row">
		<div class="col-md-7 col-lg-6">

			<div class="card">
				<div class="card-body">
					<form action="{{ route('movies.store') }}" method="post">
						@csrf
						<div class="form-group">
							<label for="titleInput">Title</label>
							<input value="{{ old('title') }}" name="title" type="text"
							class="form-control @error('title') is-invalid @enderror" id="titleInput" aria-describedby="titleHelp">
							<small id="titleHelp" class="form-text text-muted">This is the title for the movie you are creating</small>

							@error('title')
								<div class="invalid-feedback">
									{{ $message }}
								</div>
							@enderror
						</div>
						<div class="form-group">
							<label for="descriptionInput">Description</label>
							<input value="{{ old('description') }}" name="description" type="text"
							class="form-control @error('description') is-invalid @enderror" id="descriptionInput">

							@error('description')
								<div class="invalid-feedback">
									{{ $message }}
								</div>
							@enderror
						</div>
						<button type="submit" class="btn btn-primary">Create</button>
					</form>
				</div>
			</div>


		</div>
		<div class="col-md-5 col-lg-6">
			<div class="alert alert-info">
				<h3>
					Movies are awesome!
				</h3>
				<p>
					Use this form to record a new movie in our DB!
				</p>
			</div>
		</div>
	</div>


@endsection
