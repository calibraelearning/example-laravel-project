@extends('layouts/master')

@section('content')

	{{ $movies->links() }}

	@foreach($movies as $movie)
		
		<h1>
			<a href="{{ $movie->url() }}">
				{{ $movie->title }}
			</a>
		</h1>
		<p>
			{{ $movie->description }}
		</p>


	@endforeach

@endsection