@extends('layouts/master')

@section('content')

	<div class="row">
		<div class="col-md-3">
			<div class="card">
				<img src="{{ $movie->poster }}" class="card-img-top" alt="Movie poster">

				<div class="card-body">
					<h2>
						{{ $movie->title }}
					</h2>

					<p>
						{{ $movie->description }}
					</p>
				</div>
			</div>
		</div>
		<div class="col-md-6">

			<h3>
				Ratings
			</h3>
			<h5>
				This was rated {{ intval($movie->ratings()->avg('score')) }} out of {{ $movie->ratings->count() }} total ratings
			</h5>

			<p>
				All users that rated this movie:
				@foreach($movie->ratings as $rating)
					{{ $rating->user->name }}
				@endforeach
			</p>

			<form method="post" action="{{ route('ratings.store', $movie) }}">
				@csrf
				<select name="score">
					<option value="1">Hated it</option>
					<option value="2">Disliked it</option>
					<option value="3">Meh</option>
					<option value="4">Liked it</option>
					<option value="5">Loved it</option>
				</select>

				<input type="submit" value="Rate this movie!">
			</form>

			<h3>
				Cast
			</h3>

			@foreach($movie->actors as $actor)
				<h5>
					{{ $actor->name }}
				</h5>
				<h5>
					This actor has starred in {{ $actor->movies->count() }} movies
				</h5>

				@foreach($actor->movies as $otherMovie)
					<p>
						<a href="{{ $otherMovie->url() }}">
							{{ $otherMovie->title }}
						</a>
					</p>
				@endforeach

                <a href="{{ route('actors.show', $actor) }}">
				    <img src="{{ $actor->pic }}" alt="Picture of {{ $actor->full_name }}">
                </a>
			@endforeach
		</div>
	</div>

@endsection
