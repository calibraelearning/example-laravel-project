<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Movie;
use App\Models\Rating;
use Illuminate\Database\Seeder;

class RatingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$movies = Movie::all();
        foreach(User::all() as $user)
        {
        	$numRatings = rand(0, 10);
        	for($i = 0; $i < $numRatings; $i++)
        	{
        		Rating::create([
        			'user_id' => $user->id,
        			'movie_id' => $movies->random()->id,
        			'score' => rand(0, 5)
        		]);
        	}
        }
    }
}






