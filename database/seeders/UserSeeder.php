<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Faker\Factory;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Factory::create();
        for($i = 0; $i < 1000; $i++)
        {
        	User::create([
        		'name' => $faker->name,
        		'email' => $faker->email,
        		'password' => Hash::make('password')
        	]);
        }
    }
}
