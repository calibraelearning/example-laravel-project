<?php

namespace Database\Seeders;

use App\Models\Actor;
use Illuminate\Database\Seeder;

class ActorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         Actor::create([
         	'name' => 'Simon Pegg',
         	'pic' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Simon_Pegg_Premiere_of_Kill_Me_Three_Times_%28cropped%29.jpg/440px-Simon_Pegg_Premiere_of_Kill_Me_Three_Times_%28cropped%29.jpg'
         ]);

        // Link Simon Pegg to movie 1
        $actor = Actor::where('name', 'Simon Pegg')->first();

        $actor->movies()->attach(1);
        $actor->movies()->attach(2);
    }
}





