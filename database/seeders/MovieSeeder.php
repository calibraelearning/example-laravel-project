<?php

namespace Database\Seeders;

use App\Models\Movie;
use Illuminate\Database\Seeder;

class MovieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	for($i = 0; $i < 100; $i++)
    	{
    		Movie::create([
    			'title' => 'Movie title ' . $i,
    			'description' => 'Movie description ' . $i,
                'user_id' => 1
    		]);
    	}


    	// instantiate a new model
        // $newMovie = new Movie();

        // // set props
        // $newMovie->title = 'Three idiots';
        // $newMovie->description = 'Shreyas fav movie';

        // // write to the DB (SQL insert)
        // $newMovie->save();
    }
}












