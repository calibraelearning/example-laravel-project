<?php

use App\Http\Controllers\ActorController;
use App\Http\Controllers\MovieController;
use App\Http\Controllers\SearchController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('tinker', function () {
    // removed some unused code.
});

Route::get('/movies', [MovieController::class, 'index'])->name('movies.index');

// show form
Route::get('/movies/create', [MovieController::class, 'create'])->name('movies.create');

// handle new movie form
Route::post('/movies', [MovieController::class, 'store'])->name('movies.store');

Route::get('/movies/{movie}', [MovieController::class, 'show'])->name('movies.show');

Route::post('/movies/{movie}/ratings', [MovieController::class, 'storeRating'])->name('ratings.store');

Route::resource('actors', ActorController::class);

Route::get('/search', SearchController::class)->name('search');







Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return Inertia\Inertia::render('Dashboard');
})->name('dashboard');
