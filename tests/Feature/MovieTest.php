<?php

namespace Tests\Feature;

use App\Models\Actor;
use App\Models\Movie;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MovieTest extends TestCase
{

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testActorsCanOnlyBeLinkedToASingleMovieOnce()
    {
        $movie = Movie::first();
        $actor = Actor::first();

        $movie->actors()->detach();

        $this->assertEquals($movie->actors()->count(), 0);

        $this->post(route('movies.actors.attach', [$movie, $actor]))->assertOk();

        $this->assertEquals($movie->actors()->count(), 1);

        $this->post(route('movies.actors.attach', [$movie, $actor]));

        $this->assertEquals($movie->actors()->count(), 1);
    }
}
